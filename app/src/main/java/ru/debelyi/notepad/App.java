package ru.debelyi.notepad;

import android.app.Application;


import io.reactivex.annotations.NonNull;
import ru.debelyi.notepad.di.AppComponent;
import ru.debelyi.notepad.di.AppModule;
import ru.debelyi.notepad.di.DaggerAppComponent;

public class App extends Application {
    @NonNull
    AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    @NonNull
    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
