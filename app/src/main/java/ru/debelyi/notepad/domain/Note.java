package ru.debelyi.notepad.domain;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.UUID;

import ru.debelyi.notepad.data.local.NotesDao;

/**
 * Created by User on 05.06.2018.
 */

@Entity(tableName = NotesDao.TABLE_NAME, primaryKeys = {NotesDao.Columns.ID})
public class Note implements Serializable {

    @NonNull
    @ColumnInfo(name = NotesDao.Columns.ID)
    private String uid;
    @ColumnInfo(name = NotesDao.Columns.CONTENT)
    @NonNull
    private String content;
    @ColumnInfo(name = NotesDao.Columns.CREATION_TIME)
    private long creationTime;
    @ColumnInfo(name = NotesDao.Columns.EDITION_TIME)
    private long editionTime;

    @Ignore
    public Note(@NonNull String content) {
        this.uid = UUID.randomUUID().toString();
        this.content = content;
        this.creationTime = getCurrentTimeInMilliseconds();
        this.editionTime = this.creationTime;
    }

    public Note(@NonNull String uid, @NonNull String content, long creationTime, long editionTime) {
        this.uid = uid;
        this.content = content;
        this.creationTime = creationTime;
        this.editionTime = editionTime;
    }

    public void setUid(@NonNull String uid) {
        this.uid = uid;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    @NonNull
    public String getUid() {
        return uid;
    }


    @NonNull
    public String getContent() {
        return content;
    }

    public void setContent(@NonNull String content) {
        this.content = content;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public long getEditionTime() {
        return editionTime;
    }

    public void setEditionTime(long editionTime) {
        this.editionTime = editionTime;
    }

    private long getCurrentTimeInMilliseconds() {
        return System.currentTimeMillis();
    }
}
