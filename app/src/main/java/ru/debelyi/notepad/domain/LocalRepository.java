package ru.debelyi.notepad.domain;

import java.util.List;


import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;

public interface LocalRepository {
    long insertOrUpdateNote(@NonNull Note note);
    Single<Note> getNoteById(String uid);
    @NonNull
    Flowable<List<Note>> getAllNotes();
    boolean deleteNote(Note note);
}
