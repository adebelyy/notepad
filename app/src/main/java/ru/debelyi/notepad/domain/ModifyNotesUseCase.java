package ru.debelyi.notepad.domain;

import android.support.annotation.NonNull;

import io.reactivex.Completable;

public class ModifyNotesUseCase {
    @NonNull
    private final LocalRepository mLocalRepository;


    public ModifyNotesUseCase(@NonNull LocalRepository mLocalRepository) {
        this.mLocalRepository = mLocalRepository;
    }


    public long insertOrUpdate(Note note) {
        return mLocalRepository.insertOrUpdateNote(note);
    }
    public boolean deleteNote(Note note) {
        return mLocalRepository.deleteNote(note);
    }
}
