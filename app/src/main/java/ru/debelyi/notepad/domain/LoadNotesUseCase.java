package ru.debelyi.notepad.domain;

import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

public class LoadNotesUseCase {

    @NonNull
    private final LocalRepository mLocalRepository;

    public LoadNotesUseCase(@NonNull LocalRepository mLocalRepository) {
        this.mLocalRepository = mLocalRepository;
    }

    @NonNull
    public Flowable<List<Note>> getNotes() {
        return mLocalRepository.getAllNotes();
    }
}
