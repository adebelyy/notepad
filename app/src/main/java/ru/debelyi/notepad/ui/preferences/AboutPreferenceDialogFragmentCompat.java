package ru.debelyi.notepad.ui.preferences;

import android.os.Bundle;
import android.support.v7.preference.PreferenceDialogFragmentCompat;
import android.view.View;
import android.widget.TextView;

import ru.debelyi.notepad.BuildConfig;
import ru.debelyi.notepad.R;

public class AboutPreferenceDialogFragmentCompat extends PreferenceDialogFragmentCompat {

    public static AboutPreferenceDialogFragmentCompat newInstance(String key) {
        final AboutPreferenceDialogFragmentCompat fragment = new AboutPreferenceDialogFragmentCompat();
        final Bundle bundle = new Bundle();
        bundle.putString(ARG_KEY, key);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);
        String aboutText = String.format(getString(R.string.about_dialog_text), BuildConfig.VERSION_NAME);
        TextView aboutTextView = view.findViewById(R.id.dialog_about_text);
        aboutTextView.setText(aboutText);
    }

    @Override
    public void onDialogClosed(boolean positiveResult) {

    }
}
