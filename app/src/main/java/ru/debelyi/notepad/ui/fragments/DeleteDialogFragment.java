package ru.debelyi.notepad.ui.fragments;

import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ru.debelyi.notepad.R;
import ru.debelyi.notepad.domain.Note;
import ru.debelyi.notepad.ui.callbacks.DeletionCallback;
import ru.debelyi.notepad.ui.viewmodels.NotesModifyViewModel;

public class DeleteDialogFragment extends DialogFragment {

    private static final String mNoteSerializeKey = "mNoteSerializeKey";
    public static final String TAG = "NoteDeleteDialogFragm";

    @NonNull
    private NotesModifyViewModel mNotesModifyViewModel;

    @Nullable
    private Note mNote = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mNote = (Note) getArguments().getSerializable(mNoteSerializeKey);
        }

    }


    public static DeleteDialogFragment newInstance(Note note) {
        DeleteDialogFragment fragment = new DeleteDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(mNoteSerializeKey, note);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.delete_note_dialog_title)
                .setMessage(R.string.delete_note_dialog_text)
                .setPositiveButton(R.string.dialog_yes, (dialog, id) -> {
                    if (mNote != null) {
                        mNotesModifyViewModel.deleteNote(mNote);
                    }

                    ((DeletionCallback) getActivity()).doUiOperations();
                })
                .setNegativeButton(R.string.dialog_no, (dialog, id) -> {
                });
        return builder.create();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mNotesModifyViewModel = ViewModelProviders.of(this).get(NotesModifyViewModel.class);
    }
}
