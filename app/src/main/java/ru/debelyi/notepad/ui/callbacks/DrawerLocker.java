package ru.debelyi.notepad.ui.callbacks;

public interface DrawerLocker {
    public void setDrawerEnabled(boolean enabled);
}
