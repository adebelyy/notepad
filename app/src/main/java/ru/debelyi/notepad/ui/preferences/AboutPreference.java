package ru.debelyi.notepad.ui.preferences;

import android.content.Context;


import android.support.v7.preference.DialogPreference;
import android.util.AttributeSet;
import ru.debelyi.notepad.R;

public class AboutPreference extends DialogPreference {

    public AboutPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDialogLayoutResource(R.layout.dialog_about);
        setPositiveButtonText(R.string.about_dialog_ok_btn);
        setNegativeButtonText(null);
    }
}
