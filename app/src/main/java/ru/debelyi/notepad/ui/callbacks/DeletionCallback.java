package ru.debelyi.notepad.ui.callbacks;

import java.io.Serializable;

public interface DeletionCallback extends Serializable {
    void doUiOperations();
}
