package ru.debelyi.notepad.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import ru.debelyi.notepad.R;
import ru.debelyi.notepad.domain.Note;
import ru.debelyi.notepad.ui.fragments.DeleteDialogFragment;
import ru.debelyi.notepad.ui.fragments.NotesEditFragment;

public class NotesRecyclerAdapter extends RecyclerView.Adapter<NotesRecyclerAdapter.NoteViewHolder> implements Filterable {

    private SimpleDateFormat mDateFormat = new java.text.SimpleDateFormat("HH:mm dd-MM-yyyy");

    @Nullable
    private List<Note> mFilteredNotes;
    private List<Note> mNotes;
    private Context mContext;


    public NotesRecyclerAdapter() {
        super();
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new NoteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        Note note = getItem(position);
        if (note == null) {
            throw new IllegalStateException("Note might not be null");
        }
        String headerLine = note.getContent().split("\n")[0];
        holder.mNoteContent.setText(headerLine);
        holder.mNoteDatetime.setText(convertNoteTimeStampToString(note));

        holder.itemView.setOnClickListener(view -> {
            NotesEditFragment fragment = NotesEditFragment.newInstance(note);
            ((MainActivity)mContext)
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.fragment_container, fragment, NotesEditFragment.TAG)
                    .commit();
        });

        holder.itemView.setOnLongClickListener((View view) -> {
            DialogFragment newFragment = DeleteDialogFragment.newInstance(note);
            newFragment.show(((MainActivity)mContext).getSupportFragmentManager(), DeleteDialogFragment.TAG);
            return true;
        });
    }



    public void setNotes(@Nullable List<Note> notes) {
        mFilteredNotes = notes;
        mNotes = notes;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mFilteredNotes == null ? 0 : mFilteredNotes.size();
    }

    public Note getItem(int pos) {
        return mFilteredNotes == null ? null : mFilteredNotes.get(pos);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString().toLowerCase();

                if (charString.isEmpty()) {
                    mFilteredNotes = mNotes;
                }
                else {
                    List<Note> resultFilterNotes = new LinkedList<>();
                    for (Note note: mNotes) {
                        if (note.getContent().toLowerCase().contains(charString) || convertNoteTimeStampToString(note).contains(charString)) {
                            resultFilterNotes.add(note);
                        }
                    }
                    mFilteredNotes = resultFilterNotes;

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredNotes;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mFilteredNotes = (List<Note>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    static class NoteViewHolder extends RecyclerView.ViewHolder {
        @SuppressWarnings("NullableProblems")
        @NonNull
        private TextView mNoteContent;

        @SuppressWarnings("NullableProblems")
        @NonNull
        private TextView mNoteDatetime;

        NoteViewHolder(@NonNull View itemView) {
            super(itemView);
            mNoteContent = itemView.findViewById(R.id.note_text);
            mNoteDatetime = itemView.findViewById(R.id.note_datetime);
        }
    }

    private String convertNoteTimeStampToString(Note note) {
        return mDateFormat.format(note.getCreationTime());
    }


}
