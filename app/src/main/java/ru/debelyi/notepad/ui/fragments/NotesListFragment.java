package ru.debelyi.notepad.ui.fragments;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import ru.debelyi.notepad.R;
import ru.debelyi.notepad.ui.callbacks.DrawerLocker;
import ru.debelyi.notepad.ui.MainActivity;
import ru.debelyi.notepad.ui.viewmodels.NotesListViewModel;
import ru.debelyi.notepad.ui.NotesRecyclerAdapter;


public class NotesListFragment extends Fragment {

    public static final String TAG = "NotesListFragment";
    private static final String SearchStringSerializationKey = "SearchString";

    @NonNull
    private RecyclerView mNotesRecycler;


    @NonNull
    private NotesRecyclerAdapter mNotesRecyclerAdapter;


    @NonNull
    private NotesListViewModel mNotesListViewModel;

    @NonNull
    private FloatingActionButton mButtonCreate;

    @NonNull
    private TextView mEmptyListTextView;

    @NonNull
    private SearchView mSearchView;

    @NonNull
    private NavigationView mNavigationView;

    private String mSearchText;

    public static NotesListFragment newInstance() {
        return new NotesListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mSearchText = savedInstanceState.getString(SearchStringSerializationKey);
        }

        setHasOptionsMenu(true);
    }



    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mSearchView != null && mSearchView.getQuery() != null) {
            String searchQuery = mSearchView.getQuery().toString();
            outState.putSerializable(SearchStringSerializationKey, searchQuery);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rowView =  inflater.inflate(R.layout.fragment_notes_list, container, false);
        mNotesRecycler = rowView.findViewById(R.id.notes_recycler);
        mNotesRecycler.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        setHasOptionsMenu(true);
        return rowView;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).enableToolbarBackState(false);
        ((DrawerLocker) getActivity()).setDrawerEnabled(true);
        mNavigationView.post(()-> mNavigationView.setCheckedItem(R.id.nav_notes));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNotesRecycler = view.findViewById(R.id.notes_recycler);
        mNotesRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mNotesRecycler.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayout.VERTICAL));
        mNotesRecyclerAdapter = new NotesRecyclerAdapter();
        mNotesRecycler.setAdapter(mNotesRecyclerAdapter);
        mButtonCreate = view.findViewById(R.id.btn_create);
        mEmptyListTextView = view.findViewById(R.id.note_list_empty_textview);
        mNavigationView = getActivity().findViewById(R.id.nav_view);



    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mNotesListViewModel = ViewModelProviders.of(this).get(NotesListViewModel.class);
        mButtonCreate.setOnClickListener(view -> {
            Fragment notesEditFragment = NotesEditFragment.newInstance(null);
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, notesEditFragment, NotesEditFragment.TAG);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });
        getActivity().setTitle(R.string.app_name);
        subscribeUi();
    }

    private void subscribeUi() {
        mNotesListViewModel.getNotes().observe(this, notes -> {
            Log.i(TAG, "Update UI");
            if (notes == null || notes.size() == 0) {
                mNotesRecycler.setVisibility(View.GONE);
                mEmptyListTextView.setVisibility(View.VISIBLE);
            }
            else {
                mEmptyListTextView.setVisibility(View.GONE);
                mNotesRecycler.setVisibility(View.VISIBLE);
            }

            mNotesRecyclerAdapter.setNotes(notes);


        });

        mNotesListViewModel.getShowError().observe(this, isError -> {
            if (isError != null && isError) {
                Toast.makeText(getContext(), R.string.notes_load_error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem search = menu.findItem(R.id.search);
        mSearchView =  (SearchView) search.getActionView();
        search(mSearchView);

        if (!TextUtils.isEmpty(mSearchText)) {
            search.expandActionView();
            mSearchView.setQuery(mSearchText, true);
        }
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mNotesRecyclerAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }


}
