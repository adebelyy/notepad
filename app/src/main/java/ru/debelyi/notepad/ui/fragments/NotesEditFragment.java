package ru.debelyi.notepad.ui.fragments;


import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import ru.debelyi.notepad.R;
import ru.debelyi.notepad.domain.Note;
import ru.debelyi.notepad.ui.callbacks.DrawerLocker;
import ru.debelyi.notepad.ui.MainActivity;
import ru.debelyi.notepad.ui.viewmodels.NotesModifyViewModel;


public class NotesEditFragment extends Fragment {
    private static final String mNoteSerializeKey = "mNoteSerializeKey";
    private static final String mIsStateRestoredKey = "mIsStateRestoredKey";
    private static final String mIsCreationModeKey = "mCreationModeKey";
    public static final String TAG = "NotesEditFragment";

    @NonNull
    private NotesModifyViewModel mNotesModifyViewModel;

    @NonNull
    private TextInputEditText mNoteContentTextEdit;



    private Note mNote = null;
    private boolean mIsCreationMode = false;
    private boolean mIsStateRestored = false;

    public NotesEditFragment() {
        // Required empty public constructor
    }


    public static NotesEditFragment newInstance(Note note) {
        NotesEditFragment fragment = new NotesEditFragment();
        Bundle args = new Bundle();
        args.putSerializable(mNoteSerializeKey, note);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mNote = (Note) getArguments().getSerializable(mNoteSerializeKey);

        }
        setHasOptionsMenu(true);

        if (savedInstanceState != null) {
            if (mNote == null) {
                mNote = (Note) savedInstanceState.getSerializable(mNoteSerializeKey);
            }
            mIsStateRestored = savedInstanceState.getBoolean(mIsStateRestoredKey);
            mIsCreationMode = savedInstanceState.getBoolean(mIsCreationModeKey);
        }

        if (mNote == null) {
            mNote = new Note("");
            mIsCreationMode = true;
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notes_edit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mNoteContentTextEdit = view.findViewById(R.id.note_content);
        mNoteContentTextEdit.setText(mNote.getContent());


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mNotesModifyViewModel = ViewModelProviders.of(this).get(NotesModifyViewModel.class);
        afterDeviceRotationActions();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).enableToolbarBackState(true);
        ((DrawerLocker) getActivity()).setDrawerEnabled(false);
    }

    // Todo: maybe better to use view.clearFocus();
    public void closeKeyboard() {
        View view = getView();
        if (view == null) {
            Log.d(TAG, "Null view was passed to closeKeyboard()");
            return;
        }
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null)
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_edit, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                Note forDeletionDialog = null;
                if (!mIsCreationMode) {
                    forDeletionDialog = mNote;
                }
                DialogFragment newFragment = DeleteDialogFragment.newInstance(forDeletionDialog);
                newFragment.show(getActivity().getSupportFragmentManager(), DeleteDialogFragment.TAG);
                return true;
            case R.id.action_share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = mNote.getContent();
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.action_share)));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void popBackStack() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack();
    }

    /**
     * This method deletes previously created note in creationMode after screen rotation.
     * This note was created in onStop() method while device rotation process was working.
     */
    private void afterDeviceRotationActions() {
        if (mIsStateRestored && mIsCreationMode) {
            mNotesModifyViewModel.deleteNote(mNote);
            mIsStateRestored = false;
        }
    }

    private void updateData() {
        String content = mNoteContentTextEdit.getText().toString();
        if (content.trim().length() == 0) {
            if (!mIsCreationMode) {
                mNotesModifyViewModel.deleteNote(mNote);
            }
            return;
        }
        mNote.setContent(content);

        Long currentTimeInMillis = System.currentTimeMillis();
        if (mIsCreationMode) {
            mNote.setCreationTime(currentTimeInMillis);
        }
        mNote.setEditionTime(currentTimeInMillis);
        mNotesModifyViewModel.insertOrUpdateNote(mNote);
    }

    @Override
    public void onStop() {
        super.onStop();
        updateData();
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mIsStateRestored = true;
        outState.putBoolean(mIsStateRestoredKey, mIsStateRestored);
        outState.putBoolean(mIsCreationModeKey, mIsCreationMode);
        outState.putSerializable(mNoteSerializeKey, mNote);
    }
}
