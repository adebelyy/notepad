package ru.debelyi.notepad.ui.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.debelyi.notepad.App;
import ru.debelyi.notepad.domain.ModifyNotesUseCase;

import ru.debelyi.notepad.domain.Note;

public class NotesModifyViewModel extends AndroidViewModel {
    private static final String TAG = "NotesModifyViewModel";

    @Inject
    @NonNull
    public ModifyNotesUseCase mModifyNotesUseCase;

    @NonNull
    private final MutableLiveData<Boolean> mActionCompleted = new MutableLiveData<>();

    @NonNull
    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();


    public NotesModifyViewModel(@NonNull Application application) {
        super(application);
        ((App) application).getAppComponent().inject(this);
        mActionCompleted.postValue(false);

    }

    public void insertOrUpdateNote(Note note) {
        Disposable disposable = Single.fromCallable(() -> mModifyNotesUseCase.insertOrUpdate(note))
                .subscribeOn(Schedulers.io())
                .subscribe(id -> {}, throwable -> Log.e(TAG, "NotesListViewModel: failed to load notes", throwable));


        mCompositeDisposable.add(disposable);

    }

    public void deleteNote(Note note) {

        Disposable disposable = Single.fromCallable(() -> mModifyNotesUseCase.deleteNote(note))
                .subscribeOn(Schedulers.io())
                .subscribe(id -> {}, throwable -> Log.e(TAG, "NotesListViewModel: failed to delete note", throwable));

        mCompositeDisposable.add(disposable);
    }

    public void setActionCompleted(boolean value) {
        mActionCompleted.postValue(value);
    }


    @NonNull
    public LiveData<Boolean> getActionCompleted() {
        return mActionCompleted;
    }

    @Override
    protected void onCleared() {
        mCompositeDisposable.dispose();
    }
}



