package ru.debelyi.notepad.ui.fragments;


import android.app.Dialog;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import ru.debelyi.notepad.BuildConfig;
import ru.debelyi.notepad.R;

@Deprecated
public class AboutDialogFragment extends DialogFragment {
    public static final String TAG = "AboutDialogFragment";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_about, null);

        Resources res = getResources();
        String aboutText = String.format(res.getString(R.string.about_dialog_text), BuildConfig.VERSION_NAME);
        TextView aboutTextView = view.findViewById(R.id.dialog_about_text);
        aboutTextView.setText(aboutText);

        builder.setView(view)
                .setPositiveButton(R.string.about_dialog_ok_btn, (dialog, id) -> {
                    // FIRE ZE MISSILES!
                });
        return builder.create();
    }
}
