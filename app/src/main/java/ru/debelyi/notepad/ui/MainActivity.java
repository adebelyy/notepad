package ru.debelyi.notepad.ui;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;


import java.util.Locale;

import ru.debelyi.notepad.R;
import ru.debelyi.notepad.ui.callbacks.DeletionCallback;
import ru.debelyi.notepad.ui.callbacks.DrawerLocker;
import ru.debelyi.notepad.ui.fragments.AboutDialogFragment;
import ru.debelyi.notepad.ui.fragments.NotesEditFragment;
import ru.debelyi.notepad.ui.fragments.NotesListFragment;
import ru.debelyi.notepad.ui.preferences.SettingsPreferenceFragment;

public class MainActivity extends AppCompatActivity  implements
        NavigationView.OnNavigationItemSelectedListener,
        DeletionCallback,
        DrawerLocker,
        SharedPreferences.OnSharedPreferenceChangeListener {

    @NonNull
    private ActionBarDrawerToggle mToggle;
    @NonNull
    private DrawerLayout mDrawer;

    private boolean mToolBarNavigationListenerIsRegistered = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        NavigationView navigationView = findViewById(R.id.nav_view);
        mDrawer = findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(mToggle);
        mToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        if (savedInstanceState == null) {
            Fragment notesListFragment = NotesListFragment.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, notesListFragment, NotesListFragment.TAG);
            fragmentTransaction.commit();
        }

        setLanguage();



    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        boolean selectItem = true;

        if (id == R.id.nav_notes) {
            if (!isListOrEditFragmentAdded()) {
                Fragment notesListFragment = NotesListFragment.newInstance();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, notesListFragment, NotesListFragment.TAG);
                fragmentTransaction.commit();
            }
            selectItem = true;

        } else if (id == R.id.nav_settings) {
            Fragment settingsFragment = getSupportFragmentManager().findFragmentByTag(SettingsPreferenceFragment.TAG);
            if (settingsFragment != null && settingsFragment.isAdded()) {
                selectItem = false;
            }
            else {
                Fragment fragment = SettingsPreferenceFragment.newInstance();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.replace(R.id.fragment_container, fragment, SettingsPreferenceFragment.TAG);
                fragmentTransaction.commit();
                selectItem = true;
            }

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return selectItem;
    }

    private boolean isListOrEditFragmentAdded() {
        Fragment notesListFragment = getSupportFragmentManager().findFragmentByTag(NotesListFragment.TAG);
        Fragment notesEditFragment = getSupportFragmentManager().findFragmentByTag(NotesEditFragment.TAG);
        return (notesListFragment!= null && notesListFragment.isAdded()) || (notesEditFragment != null && notesEditFragment.isAdded());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout layout = findViewById(R.id.drawer_layout);
        if (layout.isDrawerOpen(GravityCompat.START)) {
            layout.closeDrawer(GravityCompat.START);
        }
        super.onBackPressed();
    }

    @Override
    public void doUiOperations() {
        Fragment notesEditFragment = getSupportFragmentManager().findFragmentByTag(NotesEditFragment.TAG);
        if (notesEditFragment != null && notesEditFragment.isVisible()) {
            ((NotesEditFragment) notesEditFragment).closeKeyboard();
            ((NotesEditFragment) notesEditFragment).popBackStack();
        }

    }


    // via: https://stackoverflow.com/questions/36579799/android-switch-actionbar-back-button-to-navigation-button
    public void enableToolbarBackState(boolean enable) {

        // To keep states of ActionBar and ActionBarDrawerToggle synchronized,
        // when you enable on one, you disable on the other.
        // And as you may notice, the order for this operation is disable first, then enable - VERY VERY IMPORTANT.
        if(enable) {
            mToggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if(!mToolBarNavigationListenerIsRegistered) {
                mToggle.setToolbarNavigationClickListener(v -> {
                    onBackPressed();
                });

                mToolBarNavigationListenerIsRegistered = true;
            }

        } else {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            mToggle.setDrawerIndicatorEnabled(true);
            mToggle.setToolbarNavigationClickListener(null);
            mToolBarNavigationListenerIsRegistered = false;
        }
    }


    @Override
    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        mDrawer.setDrawerLockMode(lockMode);
    }

    private void setLanguage() {
        SharedPreferences pres = PreferenceManager.getDefaultSharedPreferences(this);
        String settingsAppLanguageRes = getString(R.string.settings_app_language);
        String settingsDefaultLanguageRes = getString(R.string.settings_default_language_code);
        if (!pres.contains(settingsAppLanguageRes)) {
            pres.edit()
                    .putString(settingsAppLanguageRes, settingsDefaultLanguageRes)
                    .apply();
            // ui will be updated by updateUiLanguage() called from onSharedPreferenceChanged() in MainActivity
        }
        else {
            String currentLang = getResources().getConfiguration().locale.getLanguage();
            String preferenceLang = pres.getString(settingsAppLanguageRes, settingsDefaultLanguageRes);
            String systemLang = Locale.getDefault().getLanguage();

            if (!((preferenceLang.equals(settingsDefaultLanguageRes) && currentLang.equals(systemLang))
                    || (preferenceLang.equals(currentLang)))) {
                onSharedPreferenceChanged(pres, settingsAppLanguageRes);
            }
        }
    }

    public void updateUiLanguage(String lang) {
        if (lang.equals(getString(R.string.settings_default_language_code))) {
            lang = Locale.getDefault().getLanguage();
        }
        Resources res = getBaseContext().getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(lang));
        res.updateConfiguration(conf, dm);

        recreate();

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences pres = PreferenceManager.getDefaultSharedPreferences(this);
        pres.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences pres = PreferenceManager.getDefaultSharedPreferences(this);
        pres.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.settings_app_language))) {
            String lang = sharedPreferences.getString(getString(R.string.settings_app_language), getString(R.string.settings_default_language_code));
            updateUiLanguage(lang);
        }
    }
}
