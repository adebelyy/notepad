package ru.debelyi.notepad.ui.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.debelyi.notepad.App;
import ru.debelyi.notepad.domain.LoadNotesUseCase;
import ru.debelyi.notepad.domain.Note;

public class NotesListViewModel extends AndroidViewModel {

    private static final String TAG = "NotesListViewModel";


    @Inject
    @NonNull
    public LoadNotesUseCase mLoadNotesUseCase;

    @NonNull
    private final MutableLiveData<List<Note>> mNotesLiveData = new MutableLiveData<>();
    @NonNull
    private final MutableLiveData<Boolean> mShowLoading = new MutableLiveData<>();
    @NonNull
    private final MutableLiveData<Boolean> mShowError = new MutableLiveData<>();

    @NonNull
    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();


    public NotesListViewModel(@NonNull Application application) {
        super(application);
        ((App) application).getAppComponent().inject(this);
        loadNotes();
    }

    private void loadNotes() {
        Disposable disposable = mLoadNotesUseCase.getNotes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(notes -> {
                            Log.d(TAG, "NotesListViewModel: successfully loaded notes");
                            mNotesLiveData.postValue(notes);
                            mShowError.postValue(false);
                            mShowLoading.postValue(false);
                        },
                        throwable -> {
                            Log.e(TAG, "NotesListViewModel: failed to load notes", throwable);
                            mShowError.postValue(true);
                            mNotesLiveData.postValue(null);
                            mShowLoading.postValue(false);
                        });
        mCompositeDisposable.add(disposable);
    }


    @NonNull
    public LiveData<List<Note>> getNotes() {
        return mNotesLiveData;
    }

    @NonNull
    public LiveData<Boolean> getShowLoading() {
        return mShowLoading;
    }

    @NonNull
    public LiveData<Boolean> getShowError() {
        return mShowError;
    }


    @Override
    protected void onCleared() {
        mCompositeDisposable.dispose();
    }


}
