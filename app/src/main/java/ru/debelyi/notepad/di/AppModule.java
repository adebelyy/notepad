package ru.debelyi.notepad.di;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.debelyi.notepad.data.local.LocalRepositoryImpl;
import ru.debelyi.notepad.data.local.NotesDao;
import ru.debelyi.notepad.data.local.NotesDatabase;
import ru.debelyi.notepad.domain.ModifyNotesUseCase;
import ru.debelyi.notepad.domain.LoadNotesUseCase;
import ru.debelyi.notepad.domain.LocalRepository;

@Module
public class AppModule {

    @NonNull
    private Context mContext;

    public AppModule(@NonNull Context mContext) {
        this.mContext = mContext;
    }

    @Provides
    @Singleton
    NotesDao provideNotesDao(@NonNull NotesDatabase notesDatabase){
        return notesDatabase.notesDao();
    }

    @Provides
    @Singleton
    NotesDatabase provideNotesDatabase(){
        return Room.databaseBuilder(mContext, NotesDatabase.class, NotesDatabase.DATABASE_NAME).build();
    }

    @Provides
    @Singleton
    LocalRepository provideLocalRepository(@NonNull NotesDao notesDao){
        return new LocalRepositoryImpl(notesDao);
    }


    @Provides
    @Singleton
    LoadNotesUseCase provideLoadNotesUseCase(@NonNull LocalRepository localRepository) {
        return new LoadNotesUseCase(localRepository);
    }

    @Provides
    @Singleton
    ModifyNotesUseCase provideEditNotesUseCase(@NonNull LocalRepository localRepository) {
        return new ModifyNotesUseCase(localRepository);
    }
}
