package ru.debelyi.notepad.di;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Component;
import ru.debelyi.notepad.ui.viewmodels.NotesModifyViewModel;
import ru.debelyi.notepad.ui.viewmodels.NotesListViewModel;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    void inject(@NonNull NotesListViewModel notesListViewModel);
    void inject(@NonNull NotesModifyViewModel notesModifyViewModel);
}
