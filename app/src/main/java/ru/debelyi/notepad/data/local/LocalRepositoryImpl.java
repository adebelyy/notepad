package ru.debelyi.notepad.data.local;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.annotations.NonNull;
import ru.debelyi.notepad.domain.LocalRepository;
import ru.debelyi.notepad.domain.Note;

public class LocalRepositoryImpl implements LocalRepository {

    @NonNull
    private final NotesDao mNotesDao;

    public LocalRepositoryImpl(@NonNull NotesDao mNotesDao) {
        this.mNotesDao = mNotesDao;
    }


    @Override
    public long insertOrUpdateNote(Note note) {
        return mNotesDao.insertOrUpdateNote(note);
    }

    @Override
    public Single<Note> getNoteById(String uid) {
        return mNotesDao.getNoteById(uid);
    }

    @Override
    public Flowable<List<Note>> getAllNotes() {
        return mNotesDao.getAllNotes();
    }

    @Override
    public boolean deleteNote(Note note) {
        int affectedRows = mNotesDao.deleteNote(note);
        return affectedRows != 0;
    }


}
