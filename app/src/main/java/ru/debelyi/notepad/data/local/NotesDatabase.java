package ru.debelyi.notepad.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import ru.debelyi.notepad.domain.Note;

@Database(version =  NotesDatabase.DATABASE_VERSION, entities = {Note.class})
public abstract class NotesDatabase extends RoomDatabase {
    public static final String DATABASE_NAME    = "notes.db";
    public static final int    DATABASE_VERSION = 2;

    public abstract NotesDao notesDao();
}
