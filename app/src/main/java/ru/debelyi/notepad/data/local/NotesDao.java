package ru.debelyi.notepad.data.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.support.annotation.NonNull;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import ru.debelyi.notepad.domain.Note;

@Dao
public abstract class NotesDao {
    public static final String TABLE_NAME = "notes";

    public interface Columns {
        String ID             = "id";
        String CONTENT        = "content";
        String CREATION_TIME  = "creation_time";
        String EDITION_TIME   = "edition_time";
    }


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract long insertOrUpdateNote(Note note);


    @NonNull
    @Query("SELECT * FROM " + TABLE_NAME + " WHERE id = :uid")
    public abstract Single<Note> getNoteById(String uid);


    @NonNull
    @Query("SELECT * FROM " + TABLE_NAME + " ORDER BY " + Columns.CREATION_TIME + " DESC ")
    public abstract Flowable<List<Note>> getAllNotes();

    @Delete
    public abstract int deleteNote(Note note);
}
