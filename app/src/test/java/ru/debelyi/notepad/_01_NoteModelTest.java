package ru.debelyi.notepad;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.UUID;

import ru.debelyi.notepad.domain.Note;

import static org.junit.Assert.*;


public class _01_NoteModelTest {

    private Note mNote;
    private static final long mExpectedCreationTime = 10000000L;
    private static final long mExpectedEditionTime = 12345678L;
    private static final String mExpectedContent = "expectedContent";

    @Before
    public void runBeforeEachTest() {
        mNote = new Note("testContext");
    }

    @Test
    public void constructor_shouldSet_Content() {
        String expectedContent = mExpectedContent;
        mNote = new Note(expectedContent);
        String actualContent = mNote.getContent();

        Assert.assertEquals(expectedContent, actualContent);
    }

    @Test
    public void constructor_shouldSet_Uid_Content_CreationTime_EditionTime() {
        String expectedUid = UUID.randomUUID().toString();
        String expectedContent = mExpectedContent;
        Long expectedCreationTime = mExpectedCreationTime;
        Long expectedEditionTime = mExpectedEditionTime;
        mNote = new Note(expectedUid, expectedContent, expectedCreationTime, expectedEditionTime);
        String actualUid = mNote.getUid();
        String actualContent = mNote.getContent();
        Long actualCreationTime = mNote.getCreationTime();
        Long actualEditionTime = mNote.getEditionTime();

        Assert.assertEquals(expectedUid, actualUid);
        Assert.assertEquals(expectedContent, actualContent);
        Assert.assertEquals(expectedCreationTime, actualCreationTime);
        Assert.assertEquals(expectedEditionTime, actualEditionTime);

    }

    @Test
    public void setUid_ShouldSet_Uid() {
        String expected = UUID.randomUUID().toString();
        mNote.setUid(expected);
        String actual = mNote.getUid();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void setContent_ShouldSet_Content() {
        String expected = mExpectedContent;
        mNote.setContent(expected);
        String actual = mNote.getContent();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void setCreationTime_ShouldSet_CreationTime() {
        Long expected = mExpectedCreationTime;
        mNote.setCreationTime(expected);
        Long actual = mNote.getCreationTime();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void setEditionTime_ShouldSet_EditionTime() {
        Long expected = mExpectedEditionTime;
        mNote.setCreationTime(expected);
        Long actual = mNote.getCreationTime();
        Assert.assertEquals(expected, actual);
    }
}