package ru.debelyi.notepad;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.util.Log;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import java.io.IOException;

import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ru.debelyi.notepad.data.local.NotesDao;
import ru.debelyi.notepad.data.local.NotesDatabase;
import ru.debelyi.notepad.domain.Note;


@RunWith(RobolectricTestRunner.class)
public class _02_DataBaseTest {

    private NotesDatabase mDb;
    private NotesDao mDao;
    private Note mNote;

    private void setNote(Note note) {
        mNote = note;
    }

    @Before
    public void createDb() {
        Context context = RuntimeEnvironment.systemContext;
        mDb = Room.inMemoryDatabaseBuilder(context, NotesDatabase.class).build();
        mDao = mDb.notesDao();
        mNote = null;
    }


    @After
    public void closeDb() throws IOException {
        mDb.close();
    }

    @Test
    public void insertNoteAndReadInList() {
        Note note = new Note("");
        Disposable disposableInsert = Single.fromCallable(() -> mDao.insertOrUpdateNote(note))
                .subscribe(id -> {}, Throwable::new);


        Disposable disposableGet = mDao.getAllNotes().subscribe(notes -> {
            Note receivedNote = notes.get(0);
            Assert.assertEquals(note.getUid(), receivedNote.getUid());
        });


        disposableInsert.dispose();
        disposableGet.dispose();
    }

    @Test
    public void insertNoteAndFindByUid() {
        Note note = new Note("");
        Disposable disposableInsert = Single.fromCallable(() -> mDao.insertOrUpdateNote(note))
                .subscribe(id -> {}, Throwable::new);


        Disposable disposableGet = mDao.getNoteById(note.getUid())
                .subscribeOn(Schedulers.io())
                .subscribe(receivedNote -> {
            Assert.assertEquals(note.getUid(), receivedNote.getUid());
        });


        disposableInsert.dispose();
        disposableGet.dispose();
    }


    @Test
    public void insert_then_update_then_read_Note_InList() {
        Note note = new Note("");
        String expectedContent = "helloworld";

        Disposable disposableInsert = Single.fromCallable(() -> mDao.insertOrUpdateNote(note))
                .subscribe(id -> {}, Throwable::new);

        note.setContent(expectedContent);

        Disposable disposableUpdate = Single.fromCallable(() -> mDao.insertOrUpdateNote(note))
                .subscribe(id -> {}, Throwable::new);


        Disposable disposableGet = mDao.getAllNotes().subscribe(notes -> {
            Note receivedNote = notes.get(0);
            Assert.assertEquals(note.getUid(), receivedNote.getUid());
            Assert.assertEquals(note.getContent(), receivedNote.getContent());
        });

        disposableInsert.dispose();
        disposableUpdate.dispose();
        disposableGet.dispose();
    }


}
