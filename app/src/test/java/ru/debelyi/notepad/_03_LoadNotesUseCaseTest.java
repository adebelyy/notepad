package ru.debelyi.notepad;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import ru.debelyi.notepad.data.local.LocalRepositoryImpl;
import ru.debelyi.notepad.data.local.NotesDao;
import ru.debelyi.notepad.domain.LoadNotesUseCase;
import ru.debelyi.notepad.domain.Note;

public class _03_LoadNotesUseCaseTest {
    private NotesDao mDao;
    private LoadNotesUseCase mLoadNotesUseCase;

    @Before
    public void runBeforeAnyTest() {
        mDao = Mockito.mock(NotesDao.class);
        mLoadNotesUseCase = new LoadNotesUseCase(new LocalRepositoryImpl(mDao));
    }

    @Test
    public void getNotes_worksCorrectly() {
        String expectedContent = "testContent";
        List<Note> list = new LinkedList<>();
        list.add(new Note(expectedContent));
        Flowable<List<Note>> expected = Flowable.fromCallable(() -> list);
        Mockito.when(mDao.getAllNotes()).thenReturn(expected);

        Disposable disposable = mLoadNotesUseCase.getNotes().subscribe(notes -> {
            String actualContent = notes.get(0).getContent();
            Assert.assertEquals(expectedContent, actualContent);
        });

        Mockito.verify(mDao).getAllNotes();

        disposable.dispose();
    }
}
