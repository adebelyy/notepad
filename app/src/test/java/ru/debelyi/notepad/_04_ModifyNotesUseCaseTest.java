package ru.debelyi.notepad;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import ru.debelyi.notepad.data.local.LocalRepositoryImpl;
import ru.debelyi.notepad.data.local.NotesDao;
import ru.debelyi.notepad.domain.ModifyNotesUseCase;
import ru.debelyi.notepad.domain.Note;

import static org.mockito.Mockito.times;

public class _04_ModifyNotesUseCaseTest {
    private NotesDao mDao;
    private ModifyNotesUseCase mModifyNotesUseCase;

    @Before
    public void runBeforeAnyTest() {
        mDao = Mockito.mock(NotesDao.class);
        mModifyNotesUseCase = new ModifyNotesUseCase(new LocalRepositoryImpl(mDao));
    }

    @Test
    public void deleteNote_worksCorrectly() {
        int affectedRows = 1;
        Note note = new Note("");

        Mockito.when(mDao.deleteNote(note)).thenReturn(affectedRows);
        mModifyNotesUseCase.deleteNote(note);
        Mockito.verify(mDao).deleteNote(note);
    }

    @Test
    public void insertOrUpdateNote_worksCorrectly() {
        long affectedRows = 0;
        Note note = new Note("");
        Mockito.when(mDao.insertOrUpdateNote(note)).thenReturn(affectedRows);

        mModifyNotesUseCase.insertOrUpdate(note);
        note.setContent("hello world");
        mModifyNotesUseCase.insertOrUpdate(note);

        Mockito.verify(mDao, times(2)).insertOrUpdateNote(note);
    }
}
